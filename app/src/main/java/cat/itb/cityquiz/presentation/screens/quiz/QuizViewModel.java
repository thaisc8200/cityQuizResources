package cat.itb.cityquiz.presentation.screens.quiz;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.Objects;

import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.repository.AnswerCountDownTimer;
import cat.itb.cityquiz.repository.GameLogic;
import cat.itb.cityquiz.repository.RepositoriesFactory;

public class QuizViewModel extends ViewModel {
    GameLogic gameLogic = RepositoriesFactory.getGameLogic();
    AnswerCountDownTimer answerCountDownTimer = new AnswerCountDownTimer(Game.MAX_MILIS_PER_ANSWER);
    MutableLiveData<Game> game = new MutableLiveData<>();
    MutableLiveData<Integer> counter = new MutableLiveData<>();

    public void startQuiz() {
        game.postValue(gameLogic.createGame(Game.maxQuestions,Game.possibleAnswers));
    }

    public void answerQuestion(int option){
        game.postValue(gameLogic.answerQuestions(game.getValue(),option));
    }

    public LiveData<Game> getGame() {
        return game;
    }

    public int getNumCorrectAnswers() {
        return game.getValue().getScore();
    }

    public void skipQuestion(){
        game.postValue(gameLogic.skipQuestion(game.getValue()));
    }


    public LiveData<Integer> getCounter(){
        return counter;
    }

    public void stopTimer(){
        answerCountDownTimer.stop();
    }

    public void startTimer(){
        answerCountDownTimer.start();
    }

    public void setTimerChangedListener(AnswerCountDownTimer.TimerChangedListener timerChangedListener){
        answerCountDownTimer.setTimerChangedListener(timerChangedListener);
    }
}
