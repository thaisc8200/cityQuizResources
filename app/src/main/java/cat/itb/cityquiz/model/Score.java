package cat.itb.cityquiz.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity (tableName = "scores")
public class Score {
    @PrimaryKey (autoGenerate = true)
    private int id;
    private int score;

    public Score(int score) {
        this.score = score;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Score{" +
                "id=" + id +
                ", score=" + score +
                '}';
    }
}
