package cat.itb.cityquiz.data.unsplashapi.imagedownloader;

import android.os.Build;


import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import androidx.annotation.RequiresApi;
import cat.itb.cityquiz.data.citiyfiledata.CitiesFileSource;
import cat.itb.cityquiz.data.unsplashapi.UnsplashClient;
import cat.itb.cityquiz.data.unsplashapi.UnsplashInterface;
import cat.itb.cityquiz.data.unsplashapi.model.Photo;
import cat.itb.cityquiz.data.unsplashapi.model.SearchResults;
import cat.itb.cityquiz.domain.City;

public class ImagesDownloader {
    private static final boolean doNotUpdatePhotos = false;
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void download() throws IOException {
        UnsplashInterface dataService = UnsplashClient.getUnsplashClient().create(UnsplashInterface.class);
        CitiesFileSource citiesFileSource = new CitiesFileSource();
        List<City> cities = citiesFileSource.getCities();
        for(City city: cities){
            String file = toFileName(city);
            if(doNotUpdatePhotos && getPath(file).toFile().exists()){
                continue;
            }
            String query = city.getName()+", "+city.getCountry();
            SearchResults searchResults = dataService.searchPhotos(query, 0, 1, null).execute().body();
            Photo photo = searchResults.getResults().get(0);
            String url = photo.getUrls().getRegular();
            System.out.println(url);
            downloadImage(url, file);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void downloadImage(String url, String file) throws IOException {
        try(InputStream in = new URL(url).openStream()){
            Path path = getPath(file);
            System.out.println(path);
            Files.copy(in, path);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private Path getPath(String file) {
        return Paths.get("src/main/res/drawable-nodpi", file);
    }

    public static final String toFileName(City city){
        return scapeName(city.getName())+".jpg";
    }

    public static final String scapeName(String name){
        return name.replaceAll("\\W+", "").toLowerCase();
    }
}
