package cat.itb.cityquiz.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import cat.itb.cityquiz.model.Score;

@Dao
public interface ScoreDAO {
    @Insert
    void insert(Score score);

    @Query("SELECT * FROM scores")
    LiveData<List<Score>> getAllScores();

}
