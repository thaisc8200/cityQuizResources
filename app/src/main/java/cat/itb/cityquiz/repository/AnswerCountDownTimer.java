package cat.itb.cityquiz.repository;

import android.os.SystemClock;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

public class AnswerCountDownTimer {
    private static final int UPDATE_INTERVAL = 50;
    long mInitialTime;
    Timer timer;
    int maxTime;

    TimerChangedListener timerChangedListener;

    public AnswerCountDownTimer(int maxTime) {
        this.maxTime = maxTime;
    }

    public void setTimerChangedListener(TimerChangedListener timerChangedListener) {
        this.timerChangedListener = timerChangedListener;
    }

    public void stop(){
        if(timer !=null){
            timer.cancel();
            timer = null;
        }
    }

    public void start() {
        stop();
        timer = new Timer();
        mInitialTime = SystemClock.elapsedRealtime();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                timerChanged();
            }
        }, UPDATE_INTERVAL, UPDATE_INTERVAL);
    }

    public void restart() {
        start();
    }

    protected void timerChanged(){
        final long elapsedMilis =  ((SystemClock.elapsedRealtime() - mInitialTime) );
        final int remainingTime = Math.max(0,(int) (maxTime - elapsedMilis));

        if(remainingTime==0) {
            stop();
        }

        timerChangedListener.onTimeChanged(remainingTime);
    }


    public interface TimerChangedListener {
        void onTimeChanged(int milisLeft);
    }
}
